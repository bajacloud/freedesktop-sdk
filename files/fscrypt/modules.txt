# github.com/BurntSushi/toml v0.4.1
github.com/BurntSushi/toml
github.com/BurntSushi/toml/internal
# github.com/client9/misspell v0.3.4
## explicit
github.com/client9/misspell
github.com/client9/misspell/cmd/misspell
# github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d
github.com/cpuguy83/go-md2man/v2/md2man
# github.com/pkg/errors v0.9.1
## explicit
github.com/pkg/errors
# github.com/russross/blackfriday/v2 v2.0.1
github.com/russross/blackfriday/v2
# github.com/shurcooL/sanitized_anchor_name v1.0.0
github.com/shurcooL/sanitized_anchor_name
# github.com/urfave/cli v1.22.5
## explicit
github.com/urfave/cli
# github.com/wadey/gocovmerge v0.0.0-20160331181800-b5bfa59ec0ad
## explicit
github.com/wadey/gocovmerge
# golang.org/x/crypto v0.0.0-20220408190544-5352b0902921
## explicit
golang.org/x/crypto/argon2
golang.org/x/crypto/blake2b
golang.org/x/crypto/hkdf
# golang.org/x/exp/typeparams v0.0.0-20220218215828-6cf2b201936e
golang.org/x/exp/typeparams
# golang.org/x/lint v0.0.0-20210508222113-6edffad5e616
## explicit
golang.org/x/lint
golang.org/x/lint/golint
# golang.org/x/mod v0.6.0-dev.0.20220106191415-9b9b3d81d5e3
golang.org/x/mod/internal/lazyregexp
golang.org/x/mod/module
golang.org/x/mod/semver
# golang.org/x/sys v0.0.0-20220408201424-a24fb2fb8a0f
## explicit
golang.org/x/sys/cpu
golang.org/x/sys/execabs
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/plan9
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
## explicit
golang.org/x/term
# golang.org/x/tools v0.1.11-0.20220316014157-77aa08bb151a
## explicit
golang.org/x/tools/cmd/goimports
golang.org/x/tools/cover
golang.org/x/tools/go/analysis
golang.org/x/tools/go/analysis/passes/inspect
golang.org/x/tools/go/ast/astutil
golang.org/x/tools/go/ast/inspector
golang.org/x/tools/go/buildutil
golang.org/x/tools/go/gcexportdata
golang.org/x/tools/go/internal/cgo
golang.org/x/tools/go/internal/gcimporter
golang.org/x/tools/go/internal/packagesdriver
golang.org/x/tools/go/loader
golang.org/x/tools/go/packages
golang.org/x/tools/go/types/objectpath
golang.org/x/tools/go/types/typeutil
golang.org/x/tools/internal/analysisinternal
golang.org/x/tools/internal/event
golang.org/x/tools/internal/event/core
golang.org/x/tools/internal/event/keys
golang.org/x/tools/internal/event/label
golang.org/x/tools/internal/fastwalk
golang.org/x/tools/internal/gocommand
golang.org/x/tools/internal/gopathwalk
golang.org/x/tools/internal/imports
golang.org/x/tools/internal/lsp/fuzzy
golang.org/x/tools/internal/packagesinternal
golang.org/x/tools/internal/typeparams
golang.org/x/tools/internal/typesinternal
# golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
golang.org/x/xerrors
golang.org/x/xerrors/internal
# google.golang.org/protobuf v1.28.0
## explicit
google.golang.org/protobuf/cmd/protoc-gen-go
google.golang.org/protobuf/cmd/protoc-gen-go/internal_gengo
google.golang.org/protobuf/compiler/protogen
google.golang.org/protobuf/encoding/protojson
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/json
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
google.golang.org/protobuf/types/pluginpb
# honnef.co/go/tools v0.3.0
## explicit
honnef.co/go/tools/analysis/code
honnef.co/go/tools/analysis/edit
honnef.co/go/tools/analysis/facts
honnef.co/go/tools/analysis/facts/nilness
honnef.co/go/tools/analysis/facts/typedness
honnef.co/go/tools/analysis/lint
honnef.co/go/tools/analysis/report
honnef.co/go/tools/cmd/staticcheck
honnef.co/go/tools/config
honnef.co/go/tools/go/ast/astutil
honnef.co/go/tools/go/buildid
honnef.co/go/tools/go/ir
honnef.co/go/tools/go/ir/irutil
honnef.co/go/tools/go/loader
honnef.co/go/tools/go/types/typeutil
honnef.co/go/tools/internal/passes/buildir
honnef.co/go/tools/internal/renameio
honnef.co/go/tools/internal/robustio
honnef.co/go/tools/internal/sharedcheck
honnef.co/go/tools/internal/sync
honnef.co/go/tools/knowledge
honnef.co/go/tools/lintcmd
honnef.co/go/tools/lintcmd/cache
honnef.co/go/tools/lintcmd/runner
honnef.co/go/tools/lintcmd/version
honnef.co/go/tools/pattern
honnef.co/go/tools/printf
honnef.co/go/tools/quickfix
honnef.co/go/tools/sarif
honnef.co/go/tools/simple
honnef.co/go/tools/staticcheck
honnef.co/go/tools/staticcheck/fakejson
honnef.co/go/tools/staticcheck/fakereflect
honnef.co/go/tools/staticcheck/fakexml
honnef.co/go/tools/stylecheck
honnef.co/go/tools/unused
